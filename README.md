<img style="padding: 5px 25px 5px 5px;" src="./media/OJ.png" align="center" width="25%" height="25%">

# OJ

Objective-JSON is a programming language extension to JSON building on the 'everything-as-an-object' typing scheme of how my Junior JSON parser handles JSON.
This allows you to blend properties/messages (named 'queries') into your non-primitive JSON objects and run or compile them like programs.

OJ can be described as an Object Orientated Programming Language, where everything is an object, and every message/function/operation is an assignment.

Please see the documentation in the [documentation](./doc) for more information

#### Example:
```
{
    "SomeString": "foo",
    "Bar": ?
        {
            "temp_var": "foo2",
            this.SomeString.Add: temp_var,
            return: this.SomeString.Add: "foo3"
        },
    "Foo": ?
        {
            return: this.Bar
        }
}
```
