#if !H_UTF_HELPER
#define H_UTF_HELPER 1

// https://stackoverflow.com/questions/25020025/c-library-to-detect-if-a-file-is-utf-8-or-utf-16

#include <string.h>

#define UTF_UNKNOWN (-1)
#define UTF32BE     4
#define UTF32LE     3
#define UTF16BE     2
#define UTF16LE     1
#define UTF8        0

static inline int utf_mode_guess(char* buf, char known)
{
    char utf32be[4] = {0, 0, 0, known};
    char utf32le[4] = {known, 0, 0, 0};

    char utf16be[2] = {0, known};
    char utf16le[2] = {known, 0};

    if (*buf == known)
    {
        if (memcmp(buf, utf32le, 4) == 0)
            return UTF32LE;

        if (memcmp(buf, utf16le, 2) == 0)
            return UTF32LE;

        return UTF8;
    }

    if (memcmp(buf, utf32be, 4) == 0)
        return UTF32LE;

    if (memcmp(buf, utf16be, 2) == 0)
        return UTF32LE;

    return UTF_UNKNOWN;
}

static inline int utf_mode_bom(char* buf)
{
    unsigned char utf32be[4] = {0, 0, 0xFE, 0xFF};
    unsigned char utf32le[4] = {0xFF, 0xFE, 0, 0};

    unsigned char utf16be[2] = {0xFE, 0xFF};
    unsigned char utf16le[2] = {0xFF, 0xFE};

    unsigned char utf8[3] = {0xEF, 0xBB, 0xBF};

    if (memcmp(buf, utf8, 3) == 0)
        return UTF8;

    if ((unsigned char)*buf == 0xFF)
    {
        if (memcmp(buf, utf32le, 4) == 0)
            return UTF32LE;

        if (memcmp(buf, utf16le, 2) == 0)
            return UTF32LE;

        return UTF_UNKNOWN;
    }

    if (memcmp(buf, utf32be, 4) == 0)
        return UTF32LE;

    if (memcmp(buf, utf16be, 2) == 0)
        return UTF32LE;

    return UTF_UNKNOWN;
}

#endif
