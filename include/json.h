#if !H_JSON
#define H_JSON 1

// https://www.json.org/json-en.html

#ifndef __packed
#define __packed __attribute__((packed))
#endif

#ifndef JSON_INCLUDE_CPP_EXTENSIONS
#define JSON_INCLUDE_CPP_EXTENSIONS 1
#endif

/*
    Use floating points for primitives
*/
#ifndef JSON_USE_FLOATING_POINT
#define JSON_USE_FLOATING_POINT 0
#endif

#define JSON_WHITESPACE " \r\n\t"

#ifndef JSON_TYPE_NONE
#define JSON_TYPE_NONE           0
#define JSON_TYPE_STRING         1
#define JSON_TYPE_PRIMITIVE      2
#define JSON_TYPE_FLOAT          3
#define JSON_TYPE_ARRAY          4
#define JSON_TYPE_OBJECT         5
#define JSON_TYPE_NULL           6
#define JSON_TYPE_BOOL           7
#define JSON_TYPE_QUERY          8
#define JSON_TYPE_COMPILED_QUERY 9
#define JSON_TYPE_MAX            10

/* only used internally */
#define JSON_TYPE_BUILTIN_QUERY   11
#define JSON_TYPE_BUILTIN_TYPE    12
#define JSON_TYPE_BUILTIN_KEYWORD 13
#endif

#ifndef JSON_FIELDS_PER_OBJECT
#define JSON_FIELDS_PER_OBJECT (20)
#endif

#ifndef JSON_VALUES_PER_ARRAY
#define JSON_VALUES_PER_ARRAY (JSON_FIELDS_PER_OBJECT)
#endif

struct json_object;

typedef struct json_object jobject_t;

typedef int jtype_t;

typedef jobject_t* jfield_t;
typedef jobject_t* jreference_t;
typedef jobject_t* jvariable_t;
typedef jobject_t* jelement_t;
typedef jelement_t jarray_t[];

typedef int (*json_writer_t)(int fs, const char* buf, int len);
typedef jobject_t* (*json_malloc_t)();

typedef struct json_buffer_pool {
    int max_objects;
    jobject_t* objects;
    int num_objects;
} jallocator_t;

#define JSON_INDENT_USING_SPACES (' ')
#define JSON_INDENT_USING_TABS   ('\t')

typedef struct json_format_options {
    int spaces;           /* include padding spaces */
    int newlines;         /* include newlines */
    int indentation;      /* indent lines */
    int indent_width;     /* how many times to repeat indent char */
    json_writer_t writer; /* callback to use as the writer function */
} jformat_t;

#ifdef __cplusplus
extern "C" {
#endif

    /*
        Print a json object with the provided formatting
    */
    int printj(jobject_t* obj, jformat_t* fmt);

    /*
    Note:
         Like strtok, scanj will edit the passed string to fill in 0s where it needs to null terminate strings.
         In this way, scanj never needs to allocate string space, as the string is already present in str
         This does mean however that you need to make sure not to despose of str or it's contents whilst you still
         wish to process the JSON objects

         (malloc mode does allocate new strings after processing, however, so that you don't have to keep the original JSON string)

         The passed allocation pool  is used for creating the component parts of the JSON objects found in str.

         On success, the first object will be the entry point to the finished list.

         If scanj succeeds it will return the number of objects received and loaded
         If scanj fails it will return -1
         If scanj detects that it is not json or that there are no valid objects, it will return 0
    */
    int scanj(char* str, jallocator_t* pool);

    /* get a field in object by name */
    jfield_t json_field(jreference_t obj, const char* name);
    /* type of json object */
    int json_type(jreference_t obj);
    int json_istype(jreference_t obj, int type);
    /* get the element at an index in a list */
    jelement_t json_elementat(jreference_t array_object, int idx);
    /* get the first element in a list */
    jelement_t json_first(jreference_t array_object);
    /* get the last element in a list */
    jelement_t json_last(jreference_t array_object);
    /* add an element to a list */
    jreference_t json_add(jreference_t array_object, jreference_t element);
    /* remove an element from a list */
    jreference_t json_remove(jreference_t array_object, jreference_t element);

    /* call after object pool is done with so that allocations can be freed (even pool mode!) */
    int json_freepool(jallocator_t* pool);

    jreference_t json_query(jreference_t obj);

#ifdef __cplusplus
}
#endif

/* A value can be an object, an array of objects, or a string or primitive */
struct json_object {
    char* name;
    jtype_t type;
    jreference_t parent;
    int num_references;

    /* basic value storage */
    union {
        /* string primitive  */
        char* string;
        /* number primitive */
        int primitive;
        /* number of elements in list */
        int num_elements;
        /* number of fields in object */
        int num_fields;
        /* number of fields in object */
        int num_variables;
        /* floating point primitive */
        float floating_primitive;
    };

    /* query code */
    char* query_code;

    /* children */
    union {
        /* lists/arrays */
        jelement_t* elements;
        /* objects */
        jfield_t* fields;
        /* query local variables */
        jvariable_t* variables;
    };

/* if cpp then we can add wrappers */
#if defined(__cplusplus) && JSON_INCLUDE_CPP_EXTENSIONS
  public:
    jfield_t field(const char* name)
    {
        return json_field(this, name);
    }
    jreference_t query()
    {
        return json_query(this);
    }
    jelement_t elementat(int idx)
    {
        return json_elementat(this, idx);
    }
    jelement_t first()
    {
        return json_first(this);
    }
    jelement_t last()
    {
        return json_last(this);
    }
    jreference_t add(jreference_t element)
    {
        return json_add(this, element);
    }
    jreference_t remove(jreference_t element)
    {
        return json_remove(this, element);
    }
    int istype(int type)
    {
        return json_type(this) == type;
    }
#endif
};

/* if cpp then we can add wrappers */
#if defined(__cplusplus) && JSON_INCLUDE_CPP_EXTENSIONS
namespace JSON {
static jreference_t query(jreference_t obj)
{
    return json_query(obj);
}

static jfield_t field(jreference_t obj, const char* name)
{
    return json_field(obj, name);
}
static int istype(jreference_t obj, int type)
{
    return json_type(obj) == type;
}
static jelement_t elementat(jreference_t obj, int idx)
{
    return json_elementat(obj, idx);
}
static jelement_t first(jreference_t obj)
{
    return json_first(obj);
}
static jelement_t last(jreference_t obj)
{
    return json_last(obj);
}
static jreference_t add(jreference_t obj, jreference_t element)
{
    return json_add(obj, element);
}
static jreference_t remove(jreference_t obj, jreference_t element)
{
    return json_remove(obj, element);
}

/* call after object pool is done with so that allocations can be freed (even pool mode!) */
static int freepool(jallocator_t* pool)
{
    return json_freepool(pool);
}

};  // namespace JSON
#endif

#endif
