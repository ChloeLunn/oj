#include "json.h"

#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

extern void exit_with_error(int error, const char* errstr);
extern int malloc_init(jallocator_t* pool);
extern jobject_t* json_malloc();
extern int json_object_count();
static char* str;

static int isany(const int c, const char* d)
{
    char* f = (char*)d;
    while (*f)
    {
        if (c == *f++)
        {
            return 1;
        }
    }
    return 0;
}

static int isnone(const int c, const char* d)
{
    char* f = (char*)d;
    while (*f)
    {
        if (c != *f++)
        {
            return 1;
        }
    }
    return 0;
}

static void scroll_back(const char* d)
{
    while (isany(*str, d) && *str != 0)
    {
        str--;
    }
}

static void scroll_past(const char* d)
{
    while (isany(*str, d) && *str != 0)
    {
        str++;
    }
}

static void scroll_to(const char* d)
{
    while (!isany(*str, d) && *str != 0)
    {
        str++;
    }
}

static void normalise_string(char* string)
{
    char* og_string = string;
    char new_string[strlen(string) + 1];
    memset(new_string, 0, strlen(string) + 1);

    while (*string)
    {
        if (*string == '\\')
        {
            string++;

            switch (*string)
            {
                /* replace '\' with special control char
                   then load the rest of the string over the top */
            case 'b':
                {
                    strcat(new_string, "\b");
                    string++;
                    break;
                }
            case 'f':
                {
                    strcat(new_string, "\f");
                    string++;
                    break;
                }
            case 'n':
                {
                    strcat(new_string, "\n");
                    string++;
                    break;
                }
            case 'r':
                {
                    strcat(new_string, "\r");
                    string++;
                    break;
                }
            case 't':
                {
                    strcat(new_string, "\t");
                    string++;
                    break;
                }

            /* read char value as hex
               replace '\' with this value
               then load the rest of the string over the top */
            case 'u':
                {
                    unsigned char hchar;
                    unsigned char lchar;

                    /* read high then low */
                    sscanf(string, "u%2hhX", &hchar);
                    string += 3;

                    sscanf(string, "%2hhX", &lchar);
                    string += 2;

                    /* store in little endian (low then high) */
                    strncat(new_string, (char*)&lchar, 1);
                    strncat(new_string, (char*)&hchar, 1);
                    break;
                }

                /* simple escapes, just mean next char is literal so replace
                   over the top of the '\' */
            case '"':
            case '\\':
            case '/':
            default:
                {
                    strncat(new_string, string, 1);
                    string++;
                    break;
                }
            }
        }
        else
        {
            strncat(new_string, string, 1);
            string++;
        }
    }

    strcpy(og_string, new_string);
}

const char* builtin_queries[] = {
  "IsEqual",
  "IsType",
  "IsNull",
  "ToType",
  "Parent",
  "Append",
  "Remove",
  "Replace",
  "Length",
  "Contains",
  "Subtract",
  "Multiply",
  "Divide",
  "Modulo",
  "Field",
  "Count",
  "ElementAt",
  "First",
  "Last",
  "Add",
  "Remove",
  "Replace",
  "Contains",
  NULL,
};

const char* builtin_type_keywords[] = {
  "string",
  "int",
  "float",
  "bool",
  "null",
  "array",
  "object",
  "query",
  NULL,
};

const char* builtin_special_keywords[] = {
  "this",
  "system",
  "value",
  "return",
  NULL,
};

/* create a new variable */
jreference_t query_create_var(jreference_t obj, char* token_name)
{
    if (!obj->variables || obj->num_variables == 0)
        obj->variables = (jfield_t*)malloc(sizeof(jfield_t));
    else
        obj->variables = (jfield_t*)realloc(obj->variables, sizeof(jfield_t) * (obj->num_variables + 1));

    if (!obj->variables)
        return NULL;

    obj->variables[obj->num_variables] = NULL;

    obj->variables[obj->num_variables] = json_malloc();

    if (!obj->variables[obj->num_variables])
        return NULL;

    obj->num_variables++;

    /* set name and parent */
    obj->variables[obj->num_variables]->name = token_name;
    obj->variables[obj->num_variables]->parent = obj;

    /* clear other parts as this is set by rhs */
    obj->variables[obj->num_variables]->type = JSON_TYPE_NONE;
    obj->variables[obj->num_variables]->string = NULL;
    obj->variables[obj->num_variables]->num_elements = 0;
    obj->variables[obj->num_variables]->elements = NULL;

    return obj->variables[obj->num_variables];
}

/* find a variable that's already set */
jreference_t query_find_var(jreference_t obj, const char* token_name)
{
    /* try and match token to the already loaded variables */
    if (obj->variables)
    {
        for (int i = 0; i < obj->num_variables; i++)
        {
            if (strcmp(token_name, obj->variables[i]->name) == 0)
            {
                return obj->variables[i];
            }
        }
    }
    return NULL;
}

/* convert name with dot notation a token variable */
jreference_t query_dot_to_var(jreference_t obj, const char* token_name)
{
    char copy[strlen(token_name) + 1];
    memcpy(copy, token_name, strlen(token_name));

    jreference_t result = NULL;

    char* tok = strtok(copy, ".");
    int first = 1;
    if (tok)
        do
        {
            /* we have a parent */
            if (strcmp(tok, "parent") == 0)
            {
                result = result->parent;
                continue;
            }

            /* if we have a this */
            if (strcmp(tok, "this") == 0 && first)
            {
                result = obj->parent;
                first = 0;
                continue;
            }

            /* if we have a this */
            if (strcmp(tok, "system") == 0 && first)
            {
                result = NULL; /* some kind of fake object here */
                first = 0;
                continue;
            }

            jreference_t tmp = result->field(tok);
            if (tmp)
                result = tmp;

        } while (tok = strtok(NULL, "."));

    return result;
}

/* load the left hand side of a query line */
jreference_t query_load_lhs(jreference_t obj)
{
    /* skip whitespace */
    scroll_past(JSON_WHITESPACE);

    jreference_t token = NULL;
    char* token_name;
    int is_var = 0;

    /* load new variable name */
    if (*str == '"')
    {
        str++;  // skip "
        token_name = str;
string_scroll_again:
        scroll_to("\"");
        if (*(str - 1) == '\\' && *(str - 2) != '\\')
        {
            str++;
            goto string_scroll_again;
        }

        /* set null terminator during copy */
        char tc = *str;
        *str = 0;

        char* old = token_name;
        token_name = (char*)malloc((sizeof(char) * strlen(old)) + 1);
        if (!token_name)
            return NULL;
        memset(token_name, 0, (sizeof(char) * strlen(old)) + 1);
        strcpy(token_name, old);

        /* restore & move on */
        *str = tc;
        str++;

        /* undo escapes */
        normalise_string(token_name);

        scroll_to(":,}" JSON_WHITESPACE);

        is_var++;
    }
    /* load dot notation string */
    else
    {
        token_name = str;
        scroll_to(":,}" JSON_WHITESPACE);

        /* set null terminator during copy */
        char tc = *str;
        *str = 0;

        /* take copy */
        char* old = token_name;
        token_name = (char*)malloc((sizeof(char) * strlen(old)) + 1);
        if (!token_name)
            return NULL;
        memset(token_name, 0, (sizeof(char) * strlen(old)) + 1);
        strcpy(token_name, old);

        /* restore & move on */
        *str = tc;
        str++;

        /* undo escapes */
        normalise_string(token_name);
    }

    token = query_find_var(obj, token_name);

    /* discect dot notation and try to load object reference here */
    if (!token && !is_var)
    {
        token = query_dot_to_var(obj, token_name);
    }

    /* allocate variable space for new token if we still didn't find a match */
    if (!token && is_var)
    {
        token = query_create_var(obj, token_name);
    }

    return token;
}

jreference_t json_query(jreference_t obj) /* NOT parent keyword, this is actually the query's parent, so the this keyword... */
{
    if (!obj)
    {
        puts("query is null");
        return NULL;
    }

    /* if it's not a query, then we can return the OG object because there's nothing to process */
    if (obj->type != JSON_TYPE_QUERY)
    {
        return obj;
    }

    /* save old query scanner str */
    char* old_str = str;

    /* set str to this query */
    str = obj->query_code;

    /* scroll whitespace */
    scroll_past(JSON_WHITESPACE);

    int err = 0;

    do
    {
        /* allocate variable space */

        /* load token name */
        jreference_t token = query_load_lhs(obj);

        /* load rhs name/value */

        /* scroll to , or } */

        /* set flag to exit based on which found */

        /* convert rhs into JSON object or find object wiht that name */

        /* if lhs starts with '"' create local var */

        /* else traverse lhs into final function or object */

        /* do call/assign/whatever */

        /* goto top if still need to scan lines */

    } while (err > 0);

    /* restore old query str */
    str = old_str;

    puts("Query not implemented.");
    return NULL;
}
