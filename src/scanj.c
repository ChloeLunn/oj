#include "json.h"

#include <ctype.h>
#include <dirent.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <unistd.h>

extern int malloc_init(jallocator_t* pool);
extern jobject_t* json_malloc();
extern int json_object_count();

static char* str;

int recursive_open(char* dir, char* file, int flags, int mode)
{
    if (dir == NULL || file == NULL)
        return -1;

    /* try and open the file now */
    int ret = -1;

    /* save current working directory */
    char cwd[PATH_MAX];
    getcwd(cwd, PATH_MAX);

    /* change working directory to new directory */
    chdir(dir);

    /* open file */
    ret = open(file, flags, mode);

    /* if success, restore cwd and return fd */
    if (ret >= 0)
    {
        chdir(cwd);
        return ret;
    }

    /* else, scroll through subdirectories */

    DIR* dp;
    struct dirent* entry;
    struct stat statbuf;

    /* open the requested directory */
    dp = opendir(dir);

    if (dp == NULL)
    {
        return -1;
    }

    /* foreach entry */
    while ((entry = readdir(dp)) != NULL)
    {
        lstat(entry->d_name, &statbuf);

        if (S_ISDIR(statbuf.st_mode))
        {
            /* ignore hidden directories including '.' and '..' */
            if (!entry->d_name || entry->d_name[0] == '\0' || entry->d_name[0] == '.')
            {
                continue;
            }
            else
            {
                /* build new working directory string */

                char nwd[PATH_MAX];
                memset(nwd, 0, PATH_MAX);
                strcpy(nwd, dir);
                if (nwd[strlen(nwd)] != '/')
                {
                    strcat(nwd, "/");
                }
                strcat(nwd, entry->d_name);

                /* and now try to do open from here instead */
                ret = recursive_open(nwd, file, flags, mode);

                /* if we managed to open, break loop */
                if (ret >= 0)
                {
                    break;
                }
            }
        }
    }

    /* go back up */
    chdir(cwd);

    /* close directory */
    closedir(dp);

    /* return fd */
    return ret;
}

static void normalise_string(char* string)
{
    char* og_string = string;
    char new_string[strlen(string) + 1];
    memset(new_string, 0, strlen(string) + 1);

    while (*string)
    {
        if (*string == '\\')
        {
            string++;

            switch (*string)
            {
                /* replace '\' with special control char
                   then load the rest of the string over the top */
            case 'b':
                {
                    strcat(new_string, "\b");
                    string++;
                    break;
                }
            case 'f':
                {
                    strcat(new_string, "\f");
                    string++;
                    break;
                }
            case 'n':
                {
                    strcat(new_string, "\n");
                    string++;
                    break;
                }
            case 'r':
                {
                    strcat(new_string, "\r");
                    string++;
                    break;
                }
            case 't':
                {
                    strcat(new_string, "\t");
                    string++;
                    break;
                }

            /* read char value as hex
               replace '\' with this value
               then load the rest of the string over the top */
            case 'u':
                {
                    unsigned char hchar;
                    unsigned char lchar;

                    /* read high then low */
                    sscanf(string, "u%2hhX", &hchar);
                    string += 3;

                    sscanf(string, "%2hhX", &lchar);
                    string += 2;

                    /* store in little endian (low then high) */
                    strncat(new_string, (char*)&lchar, 1);
                    strncat(new_string, (char*)&hchar, 1);
                    break;
                }

                /* simple escapes, just mean next char is literal so replace
                   over the top of the '\' */
            case '"':
            case '\\':
            case '/':
            default:
                {
                    strncat(new_string, string, 1);
                    string++;
                    break;
                }
            }
        }
        else
        {
            strncat(new_string, string, 1);
            string++;
        }
    }

    strcpy(og_string, new_string);
}

static int isany(const int c, const char* d)
{
    char* f = (char*)d;
    while (*f)
    {
        if (c == *f++)
        {
            return 1;
        }
    }
    return 0;
}

static int isnone(const int c, const char* d)
{
    char* f = (char*)d;
    while (*f)
    {
        if (c != *f++)
        {
            return 1;
        }
    }
    return 0;
}

static void scroll_back(const char* d)
{
    while (isany(*str, d) && *str != 0)
    {
        str--;
    }
}

static void scroll_past(const char* d)
{
    while (isany(*str, d) && *str != 0)
    {
        str++;
    }
}

static void scroll_to(const char* d)
{
    while (!isany(*str, d) && *str != 0)
    {
        str++;
    }
}

/* forward decl */
int scanj_obj(jobject_t* object);

int scanj_value(jobject_t* value)
{
    value->type = JSON_TYPE_NONE;
    value->num_elements = 0;
    value->elements = NULL;

    int err = 0;

    /* skip whitespace */
    scroll_past(JSON_WHITESPACE);

    switch (*str++)
    {
        /* null */
    case 'n':
        {
            value->type = JSON_TYPE_NULL;
            value->primitive = 0;
            break;
        }

        /* true */
    case 't':
        {
            value->type = JSON_TYPE_BOOL;
            value->primitive = 1;
            break;
        }

        /* false */
    case 'f':
        {
            value->type = JSON_TYPE_BOOL;
            value->primitive = 0;
            break;
        }

        /* elements */
    case '[':
        {
            value->type = JSON_TYPE_ARRAY;
            value->num_elements = 0;

            do
            {
                if (!value->elements)
                    value->elements = (jelement_t*)malloc(sizeof(jelement_t));
                else
                    value->elements = (jelement_t*)realloc(value->elements, sizeof(jelement_t) * (value->num_elements + 1));

                if (!value->elements)
                    return -1;

                value->elements[value->num_elements] = json_malloc();

                if (!value->elements[value->num_elements])
                    return -1;

                value->num_elements++;

                err = scanj_value(value->elements[value->num_elements - 1]);

                value->elements[value->num_elements - 1]->parent = value;

                str++;
            } while (err > 0);

            if (err == -2)
            {
                value->num_elements--;
                err = 0;
            }

            break;
        }

        /* object */
    case '{':
        {
            value->type = JSON_TYPE_OBJECT;
            err = scanj_obj(value);
            str++;
            break;
        }

        /* string */
    case '"':
        {
            value->type = JSON_TYPE_STRING;
            /* set start */
            value->string = str;

            /* scan to end of string, ignore escaped quotes */
string_scroll_again:
            scroll_to("\"");
            if (*(str - 1) == '\\' && *(str - 2) != '\\')
            {
                str++;
                goto string_scroll_again;
            }

            /* set null terminator */
            *str = 0;
            /* move on */
            str++;

            char* old = value->string;
            value->string = (char*)malloc((sizeof(char) * strlen(old)) + 1);
            if (!value->string)
                return -1;
            memset(value->string, 0, (sizeof(char) * strlen(old)) + 1);
            strcpy(value->string, old);

            /* undo escapes */
            normalise_string(value->string);

            break;
        }
        /* queries */
    case '?':
        {
            value->type = JSON_TYPE_QUERY;
            scroll_to("{");
            str++;
            scroll_past(JSON_WHITESPACE);
            value->query_code = str;
            int brace_count = 1;
            while (brace_count)
            {
                switch (*str++)
                {
                case '{':
                    brace_count++;
                    break;
                case '}':
                    brace_count--;
                    break;
                default:
                    break;
                }
            }
            /* set null terminator over last brace */
            *(--str) = 0;

            char* cur = str;
            str--;
            scroll_back(JSON_WHITESPACE);
            str++;
            *(str) = 0;
            str = cur;

            /* move on */
            str++;

            char* old = value->query_code;
            value->query_code = (char*)malloc((sizeof(char) * strlen(old)) + 1);
            if (!value->query_code)
                return -1;
            memset(value->query_code, 0, (sizeof(char) * strlen(old)) + 1);
            strcpy(value->query_code, old);
        }
        break;

        /* compiled queries */
    case '&':
        {
            value->type = JSON_TYPE_COMPILED_QUERY;
            value->primitive = atoi(str);
        }
        break;

    /* import object from file */
    case '<':
        {
            value->type = JSON_TYPE_OBJECT;
            value->string = str;

            jreference_t par = value->parent;

            /* scan to end of string, ignore escaped quotes */
import_scroll_again:
            scroll_to(">");
            if (*(str - 1) == '\\' && *(str - 2) != '\\')
            {
                str++;
                goto string_scroll_again;
            }
            /* set null terminator */
            *str = 0;
            /* move on */
            str++;

            char* copy = (char*)malloc((sizeof(char) * strlen(value->string)) + 1);
            if (!copy)
                return -1;
            memset(copy, 0, (sizeof(char) * strlen(value->string)) + 1);
            strcpy(copy, value->string);

            /* undo escapes */
            normalise_string(copy);

            /* get current working directory */
            char cwd[PATH_MAX];
            getcwd(cwd, PATH_MAX);

            int fd = recursive_open(cwd, copy, O_RDONLY, 0);

            if (fd < 0)
            {
                return -1;
            }

            char* saved_str = str;

            size_t len = lseek(fd, 0, SEEK_END);

            str = (char*)mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_PRIVATE, fd, 0);

            err |= scanj_value(value);

            value->parent = par;

            munmap(str, len);
            close(fd);

            str = saved_str;

            break;
        }

    /* hit end of array/object with no contents */
    case ']':
    case '}':
        str--;
        return -2;

        /* either a number or some unknown */
    default:
        if (isdigit(*(str - 1)) || *(str - 1) == '-')
        {
            if (*(str - 1) == '-')
            {
                value->type = JSON_TYPE_PRIMITIVE;
                value->primitive = -atoi(str);
            }
            else
            {
                value->type = JSON_TYPE_PRIMITIVE;
                value->primitive = atoi(str - 1);
            }
        }
        else
        {
            value->type = JSON_TYPE_NULL;
            value->primitive = 0;
        }
        break;
    }

    if (err < 0)
    {
        return err;
    }

    /*
        Scroll to a separator
    */
    scroll_to("}],");

    /* hit end of object/elements */
    if (*str == '}' || *str == ']' || *str == 0)
    {
        return 0;
    }
    /* still in list */
    return 1;
}

/* scan a token */
int scanj_token(jobject_t* token)
{
    /* clear token */
    token->name = NULL;
    token->type = JSON_TYPE_NULL;
    token->string = NULL;
    token->num_elements = 0;
    token->elements = NULL;

    /* skip whitespace */
    scroll_past(JSON_WHITESPACE);

    switch (*str)
    {
        /* found an empty object */
    case '}':
        return -2;
    }

    /*scan to start of token name */
    scroll_to("\"");

    /* save start */
    str++;  // skip "
    token->name = str;

/* scan to end of string, ignore escaped quotes */
string_scroll_again:
    scroll_to("\"");
    if (*(str - 1) == '\\' && *(str - 2) != '\\')
    {
        str++;
        goto string_scroll_again;
    }

    /* set null terminator */
    *str = 0;
    /* move on */
    str++;

    char* old = token->name;
    token->name = (char*)malloc((sizeof(char) * strlen(old)) + 1);
    if (!token->name)
        return -1;
    memset(token->name, 0, (sizeof(char) * strlen(old)) + 1);
    strcpy(token->name, old);

    /* undo escapes */
    normalise_string(token->name);

    /* skip ':'s and whitespace */
    scroll_past(":" JSON_WHITESPACE);

    /* scan value in */
    return scanj_value(token);
}

int scanj_obj(jobject_t* object)
{
    object->type = JSON_TYPE_OBJECT;
    object->num_fields = 0;
    object->fields = NULL;

    /*
        increment number of used fields.
        If we've overflowed, error
        set the current token to the allocated token
        scan it
        if <=0 then no more to load or error, so return
    */
    int err = 0;
    do
    {
        if (!object->fields || object->num_fields == 0)
            object->fields = (jfield_t*)malloc(sizeof(jfield_t));
        else
            object->fields = (jfield_t*)realloc(object->fields, sizeof(jfield_t) * (object->num_fields + 1));

        if (!object->fields)
            return -1;

        object->fields[object->num_fields] = NULL;

        object->fields[object->num_fields] = json_malloc();

        if (!object->fields[object->num_fields])
            return -1;

        object->num_fields++;

        err = scanj_token(object->fields[object->num_fields - 1]);

        object->fields[object->num_fields - 1]->parent = object;

    } while (err > 0);

    if (err == -2)
    {
        object->num_fields--;
        err = 0;
    }

    return err;
}

int scanj(char* string, jallocator_t* pool)
{
    if (string)
        str = string;

    int err = 0;

    if (pool)
        malloc_init(pool);

    jobject_t* obj = json_malloc();

    if (!obj)
        return -1;

    err = scanj_obj(obj);

    if (err < 0)
        return err;

    return json_object_count();
}
