#include "json.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

jelement_t json_elementat(jreference_t array_object, int idx)
{
    if (!array_object)
        return NULL;

    if (array_object->type != JSON_TYPE_ARRAY)
        return NULL;

    return array_object->elements[idx];
}

jelement_t json_first(jreference_t array_object)
{
    if (!array_object)
        return NULL;

    if (array_object->type != JSON_TYPE_ARRAY)
        return NULL;

    return array_object->elements[0];
}

jelement_t json_last(jreference_t array_object)
{
    if (!array_object)
        return NULL;

    if (array_object->type != JSON_TYPE_ARRAY)
        return NULL;

    return array_object->elements[array_object->num_elements - 1];
}

jreference_t json_add(jreference_t array_object, jreference_t element)
{
    if (!array_object || !element)
        return NULL;

    if (array_object->type != JSON_TYPE_ARRAY)
        return NULL;

    if (element->type <= JSON_TYPE_NONE && element->type >= JSON_TYPE_MAX)
        return NULL;

    if (array_object->num_elements >= JSON_VALUES_PER_ARRAY)
        return NULL;

    array_object->elements[array_object->num_elements] = element;
    array_object->num_elements++;
    return array_object;
}

jreference_t json_remove(jreference_t array_object, jreference_t element)
{
    if (array_object->type != JSON_TYPE_ARRAY)
        return NULL;

    if (element->type <= JSON_TYPE_NONE && element->type >= JSON_TYPE_MAX)
        return NULL;

    return NULL;
}
