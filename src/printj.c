#include "json.h"

#include <ctype.h>
#include <stdarg.h>
#include <stdio.h>
#include <string.h>

static jformat_t format = {
  .spaces = 1,
  .newlines = 1,
  .indentation = JSON_INDENT_USING_SPACES,
  .indent_width = 4,
  .writer = NULL,
};
static int indent_level = 0;

int json_printf(int esc, const char* restrict fmt, ...);

int json_write(int fd, const char* buf, int len)
{
    if (format.writer)
        return format.writer(fd, buf, len);
    return -1;
};

/* print a single char */
int json_putchar(int ic)
{
    char c = ic & 0xFF;
    return json_write(1, &c, 1);
}

/* json_print a string of length */
int json_print(const char* buf, unsigned int len, int esc)
{
    for (int i = 0; i < len && buf[i]; i++)
    {
        if (esc)
        {
            /* do any needed escapes in strings */
            switch (buf[i])
            {
            case '\b':
                json_putchar('\\');
                json_putchar('b');
                break;
            case '\f':
                json_putchar('\\');
                json_putchar('f');
                break;
            case '\n':
                json_putchar('\\');
                json_putchar('n');
                break;
            case '\r':
                json_putchar('\\');
                json_putchar('r');
                break;
            case '\t':
                json_putchar('\\');
                json_putchar('t');
                break;
            case '"':
                json_putchar('\\');
                json_putchar('"');
                break;
            case '\\':
                json_putchar('\\');
                json_putchar('\\');
                break;
                // case '/': -- spec says to do this, but appears that most implementations don't...
            default:
                if (!isprint(buf[i]))
                {
                    /* print back in little endian */
                    json_printf(0, "\\u%02X", (unsigned char)buf[i + 1]);
                    json_printf(0, "%02X", (unsigned char)buf[i]);
                    i++;
                }
                else
                {
                    json_putchar(buf[i]);
                }
                break;
            }
        }
        else
        {
            json_putchar(buf[i]);
        }
    }
    return len;
}

/* print a number */
int json_printn(unsigned long n, int base, int ucase, int fbuf)
{
    char buf[16];
    char* p = &buf[16 - 1];
    static const char digs[] = "0123456789abcdef";
    static const char udigs[] = "0123456789ABCDEF";

    do
    {
        if (ucase)
        {
            *p-- = udigs[n % base];
        }
        else
        {
            *p-- = digs[n % base];
        }
        n /= base;
    } while (n != 0);

    while (++p != &buf[16])
    {
        if (json_putchar(*p) == -1)
        {
            return -1;
        }
    }

    return 0;
}

/* printf used by json */
int json_printf(int esc, const char* restrict fmt, ...)
{
    if (fmt == NULL)
        return -1;

    char s[512];
    va_list args;
    va_start(args, fmt);
    vsprintf(s, fmt, args);
    int r = json_print(s, strlen(s), esc);
    va_end(args);

    return r;
}

int json_newline()
{
    if (!format.newlines)
        return 0;

    json_putchar('\n');

    if (format.indentation)
    {
        for (int i = 0; i < indent_level; i++)
        {
            for (int j = 0; j < format.indent_width; j++)
            {
                json_putchar(format.indentation);
            }
        }
    }
    return 0;
}

/* start a json object string */
int json_start_obj()
{
    indent_level++;
    json_putchar('{');

    if (format.newlines)
    {
        json_newline();
    }
    else if (format.spaces)
    {
        json_putchar(' ');
    }
    return 0;
}

/* end a json object string */
int json_end_obj()
{
    indent_level--;
    if (format.newlines)
    {
        json_newline();
    }
    else if (format.spaces)
    {
        json_putchar(' ');
    }
    return json_putchar('}');
}

/* yeah, stupid, but reduces memory footprint... */
int json_quote()
{
    return json_putchar('"');
}

int json_delimit()
{
    json_putchar(':');
    if (format.spaces)
    {
        json_putchar(' ');
    }
    return 0;
}

int json_separate()
{
    json_putchar(',');

    if (format.newlines)
    {
        json_newline();
    }
    else if (format.spaces)
    {
        json_putchar(' ');
    }

    return 0;
}

/* start a json elements */
int json_start_array()
{
    indent_level++;

    json_putchar('[');

    if (format.newlines)
    {
        json_newline();
    }
    else if (format.spaces)
    {
        json_putchar(' ');
    }

    return 0;
}

/* end a json object string */
int json_end_array()
{
    indent_level--;

    if (format.newlines)
    {
        json_newline();
    }
    else if (format.spaces)
    {
        json_putchar(' ');
    }

    return json_putchar(']');
}

/* start a json object string */
int json_start_query()
{
    indent_level++;

    json_putchar('?');

    if (format.newlines)
    {
        json_newline();
    }
    else if (format.spaces)
    {
        json_putchar(' ');
    }

    json_start_obj();

    return 0;
}

/* end a json object string */
int json_end_query()
{
    json_end_obj();

    indent_level--;

    return 0;
}

int printj_value(jobject_t* val)
{
    if (!val)
    {
        return -1;
    }

    if (val->type <= JSON_TYPE_NONE || val->type >= JSON_TYPE_MAX)
    {
        return -1;
    }

    int err = 0;

    switch (val->type)
    {
    case JSON_TYPE_QUERY:
        {
            json_start_query();
            json_print(val->query_code, strlen(val->query_code), 0);
            json_end_query();
            break;
        }
    case JSON_TYPE_COMPILED_QUERY:
        {
            json_putchar('&');
            json_printn(val->primitive, 10, 0, 0);
            break;
        }
        /* simple boolean true/false */
    case JSON_TYPE_BOOL:
        {
            if (val->primitive)
                json_print("true", 4, 0);
            else
                json_print("false", 5, 0);
            break;
        }
        /* simple string */
    case JSON_TYPE_STRING:
        {
            json_quote();
            json_print(val->string, strlen(val->string), 1);
            json_quote();
            break;
        }
        /* primatives (ints) */
    case JSON_TYPE_PRIMITIVE:
        {
            if (val->primitive >= 0)
            {
                json_printn(val->primitive, 10, 0, 0);
            }
            else
            {
                json_putchar('-');
                json_printn(-val->primitive, 10, 0, 0);
            }
            break;
        }
        /* elements of values */
    case JSON_TYPE_ARRAY:
        {
            if (val->num_elements == 0)
            {
                json_putchar('[');
                json_putchar(']');
            }
            else
            {
                json_start_array();
                for (int i = 0; i < val->num_elements && !err; i++)
                {
                    err |= printj_value(val->elements[i]);

                    /* add separator */
                    if (i < val->num_elements - 1)
                    {
                        json_separate();
                    }
                }
                json_end_array();
            }
            break;
        }
        /* an object */
    case JSON_TYPE_OBJECT:
        {
            err |= printj(val, NULL);
            break;
        }
        /* a null/empty one or default (unknown)*/
    case JSON_TYPE_NULL:
    default:
        {
            json_print("null", 4, 0);
            break;
        }
    }

    return err;
}

int printj_tok(jobject_t* tok)
{
    if (!tok)
    {
        return -1;
    }

    if (tok->type <= JSON_TYPE_NONE || tok->type >= JSON_TYPE_MAX)
    {
        return -1;
    }

    int err = 0;

    json_quote();
    json_print(tok->name, strlen(tok->name), 1);
    json_quote();
    json_delimit();

    err |= printj_value(tok);

    return err;
}

/* print a json object with formatting options */
int printj(jobject_t* obj, jformat_t* fmt)
{
    int err = 0;

    if (fmt)
        memcpy(&format, fmt, sizeof(jformat_t));

    if (obj->type != JSON_TYPE_OBJECT)
    {
        printj_value(obj);
    }
    else if (obj->num_fields == 0)
    {
        json_putchar('{');
        json_putchar('}');
    }
    else
    {
        json_start_obj();
        for (int i = 0; i < obj->num_fields && !err; i++)
        {
            err = printj_tok(obj->fields[i]);

            /* add separator */
            if (i < obj->num_fields - 1)
            {
                json_separate();
            }
        }
        json_end_obj();
    };

    if (fmt)
    {
        int tmpnl = format.newlines;
        format.newlines = 1;
        json_newline();
        format.newlines = tmpnl;
    }

    return err;
}
