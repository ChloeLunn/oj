#include "json.h"

#include <stdlib.h>

extern jobject_t* json_malloc();
extern int json_object_count();

jreference_t json_string_add(jreference_t str, const char* append)
{
    if (!str)
        return NULL;

    if (str->type != JSON_TYPE_STRING)
        return NULL;

    return str;
}

jreference_t json_string_append(jreference_t str, const char* append)
{
    return json_string_add(str, append);
}

jreference_t json_string_remove(jreference_t str, const char* remove)
{
    return NULL;
}

jreference_t json_string_replace(jreference_t str, const char* original, const char* new_str)
{
    return NULL;
}

jreference_t json_string_length(jreference_t str)
{
    return NULL;
}

jreference_t json_string_contains(jreference_t str, const char* query)
{
    return NULL;
}
