#include "json.h"

#include <limits.h>
#include <stdlib.h>

static jallocator_t* m_pool;

static jobject_t* small_json_malloc()
{
    return &m_pool->objects[m_pool->num_objects - 1];
}

int malloc_init(jallocator_t* pool)
{
    if (pool)
    {
        m_pool = pool;
        m_pool->num_objects = 0;
        m_pool->max_objects = INT_MAX;
    }

    return 0;
}

jobject_t* json_malloc()
{
    if (m_pool->num_objects == 0)
    {
        m_pool->num_objects++;
        return &m_pool->objects[0];
    }

    m_pool->num_objects++;

    if (m_pool->num_objects > m_pool->max_objects)
        return NULL;

    return (jobject_t*)malloc(sizeof(jobject_t));
}

int json_object_count()
{
    return m_pool->num_objects;
}

int free_object(jobject_t* obj);

int free_value(jobject_t* val)
{
    if (!val)
    {
        return -1;
    }

    if (val->type <= JSON_TYPE_NONE || val->type >= JSON_TYPE_MAX)
    {
        return -1;
    }

    int err = 0;

    switch (val->type)
    {
        /* elements of values */
    case JSON_TYPE_ARRAY:
        {
            for (int i = 0; i < val->num_elements && !err; i++)
            {
                err |= free_value(val->elements[i]);
            }
            free(val->elements);
            break;
        }
        /* an object */
    case JSON_TYPE_OBJECT:
        {
            err |= free_object(val);
            break;
        }
        /* simple string */
    case JSON_TYPE_STRING:
        {
            free(val->string);
            break;
        }
        /* simple boolean true/false */
    case JSON_TYPE_BOOL:
        /* primatives (ints) */
    case JSON_TYPE_PRIMITIVE:
        /* a null/empty one or default (unknown)*/
    case JSON_TYPE_NULL:
    default:
        {
            break;
        }
    }

    return err;
}

int free_tok(jobject_t* tok)
{
    if (!tok)
    {
        return -1;
    }

    if (tok->type <= JSON_TYPE_NONE || tok->type >= JSON_TYPE_MAX)
    {
        return -1;
    }

    int err = 0;

    free(tok->name);

    err |= free_value(tok);

    return err;
}

/* print a json object with formatting options */
int free_object(jobject_t* obj)
{
    int err = 0;

    for (int i = 0; i < obj->num_fields && !err; i++)
    {
        err |= free_tok(obj->fields[i]);
    }

    free(obj->fields);

    return err;
}

int json_freepool(jallocator_t* pool)
{
    if (pool)
    {
        m_pool = pool;
        m_pool->max_objects = INT_MAX;
    }

    m_pool->num_objects = 0;

    return free_object(&m_pool->objects[0]);
}
