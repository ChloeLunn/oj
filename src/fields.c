#include "json.h"

#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

jfield_t json_field(jreference_t obj, const char* name)
{
    if (!obj)
        return NULL;

    if (obj->type != JSON_TYPE_OBJECT)
        return NULL;

    for (int i = 0; i < obj->num_fields; i++)
    {
        if (strcmp(name, obj->fields[i]->name) == 0)
        {
            return obj->fields[i];
        }
    }

    return NULL;
}

int json_type(jreference_t obj)
{
    if (!obj)
        return JSON_TYPE_NONE;

    return obj->type;
}

int json_istype(jreference_t obj, int type)
{
    if (!obj)
        return JSON_TYPE_NONE;

    return obj->type == type;
}

int json_tostring(jreference_t obj)
{
    switch (obj->type)
    {
    case JSON_TYPE_ARRAY:
        break;
    case JSON_TYPE_STRING:
        break;
    case JSON_TYPE_BOOL:
        break;
    case JSON_TYPE_NULL:
        break;
    case JSON_TYPE_OBJECT:
        break;
    case JSON_TYPE_PRIMITIVE:
        break;
    default:
        break;
    }
    return 0;
}
