#include "json.h"
#include "unistd.h"
#include "utf_helper.h"

#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/mman.h>
#include <unistd.h>

const char oj_logo[] = {
#include "../media/OJ.h"
};

#undef JSON_OBJECTS_PER_FILE
#define JSON_OBJECTS_PER_FILE 1

#ifndef OJ_PRINT_LOGO
#define OJ_PRINT_LOGO 0
#endif

/* allows wrapping json print to stdout */
int write_wrapper(int fd, const char* buf, int len)
{
    return write(STDOUT_FILENO, buf, len);
}

jobject_t jobjects[JSON_OBJECTS_PER_FILE];
jallocator_t jpool = {
  .max_objects = JSON_OBJECTS_PER_FILE,
  .objects = &jobjects[0],
  .num_objects = 0,
};

/* standard formatting */
jformat_t format = {
  .spaces = 1,
  .newlines = 1,
  .indentation = JSON_INDENT_USING_SPACES,
  .indent_width = 4,
  .writer = &write_wrapper,
};

char* json_file;
size_t len = 0;
int sfile = -1;

extern int recursive_open(char* dir, char* file, int flags, int mode);

void exit_with_error(int error, const char* errstr)
{
    if (errstr && isatty(STDERR_FILENO))
        fprintf(stderr, "%s\n", errstr);
    json_freepool(&jpool);

    if (json_file)
        munmap(json_file, len);

    if (sfile >= 0)
        close(sfile);

    exit(error);
}

int main(int argc, char** argv)
{
    setbuf(stdout, NULL);
    setbuf(stderr, NULL);

    int error = 0;
    const char* err_str = NULL;

    if (!isatty(STDIN_FILENO) && argc < 2)
    {
        sfile = STDIN_FILENO;
        len = lseek(sfile, 0, SEEK_END);
        json_file = (char*)mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_PRIVATE, sfile, 0);
    }
    else
    {
        if (argc < 2)
        {
            exit_with_error(1, "usage: oj [source file]");
        }

        /* could probably be converted to a regular open, but this is fine */
        char cwd[1024];
        getcwd(cwd, 1024);
        sfile = recursive_open(cwd, argv[1], O_RDONLY, 0);

        if (sfile < 0)
        {
            exit_with_error(1, "Unable to open input file");
        }

        len = lseek(sfile, 0, SEEK_END);
        json_file = (char*)mmap(NULL, len, PROT_READ | PROT_WRITE, MAP_PRIVATE, sfile, 0);
    }

    if (!json_file)
    {
        exit_with_error(1, "Unable to open input file");
    }

    if (OJ_PRINT_LOGO && isatty(STDOUT_FILENO))
    {
        /* print logo */
        system("clear");
        write(STDOUT_FILENO, "\x1b[1m", strlen("\x1b[1m"));
        write(STDOUT_FILENO, "\x1b[38;5;214m", strlen("\x1b[38;5;214m"));
        write(STDOUT_FILENO, oj_logo, sizeof(oj_logo));
        write(STDOUT_FILENO, "\x1b[0m", strlen("\x1b[0m"));
        puts("");
    }

    /* load file into json objects */
    int r = scanj(json_file, &jpool);

    if (r <= 0)
    {
        exit_with_error(1, "Error loading source.");
    }

    jreference_t entry = NULL;
    entry = jpool.objects->field("Entry");
    if (!entry)
        entry = jpool.objects->field("ENTRY");
    if (!entry)
        entry = jpool.objects->field("entry");
    if (!entry)
        entry = jpool.objects->field("Main");
    if (!entry)
        entry = jpool.objects->field("MAIN");
    if (!entry)
        entry = jpool.objects->field("main");

    if (!entry)
    {
        exit_with_error(1, "No reference to entry or main found.");
    }

    jreference_t ret = entry->query();

    if (!ret)
    {
        exit_with_error(1, "Error in query");
    }

    if (ret->type == JSON_TYPE_PRIMITIVE)
    {
        error = ret->primitive;
    }
    else if (ret->type == JSON_TYPE_BOOL)
    {
        if (ret->primitive)
            error = EXIT_SUCCESS;
        else
            error = EXIT_FAILURE;
    }
    else if (ret->type == JSON_TYPE_FLOAT)
    {
        error = (int)ret->floating_primitive;
    }
    else if (ret->type == JSON_TYPE_STRING)
    {
        error = 0;
        printf("%s\n", ret->string);
    }
    else
    {
        error = printj(ret, &format);
    }

    exit_with_error(error, NULL);
}
