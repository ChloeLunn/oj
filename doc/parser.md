# JSON Parsing/Handling

## Comments

The OJ JSON loader does not support comments.

## Fields/Tokens/Keys/Variables

Fields can be of any type. 

Unlike regular JSON, OJ field/variable names must follow the de facto standard as used by many other programming languages: names must contain only upper or lowercase alphanumeric or '_' characters and must not start with a numerical character.

## Types

As per the Junior implementation of JSON, everything in OJ is in some way a generic object, even queries.
Or if you prefer, everything is a Query as syntax-ly there is no difference in their invocation.

Types are inferred by the first non-whitespace character after the ':' in object declarations, the same as in regular JSON.

| Type           | Character     |
| -------------- | ------------- |
| String         | `"`           |
| Primitive      | (some number) |
| Boolean        | `t` or `f`    |
| Null           | `n`           |
| Array/List     | `[` to `]`    |
| Object         | `{` to `}`    |
| Query          | `?`+`{` to `}`|
| Import/include | `<` to `>`    |

Any unrecognised type character is defaulted to null.

### Strings, Primitives, Booleans, and Null

When processing to top level JSON or imported file, it is assumed that booleans and null will spell out the whole word (`true`, `false`, or `null`) just from the first letters, however this is not processed and only the first letter is used to determine type (and value in the booleans). Inside a query, the whole string is checked as you may be using a variable or keyword that starts with one of those letters.

Strings are a full implementation, processed as per the [JSON spec](https://www.json.org/json-en.html) or [RFC 7159](https://www.ietf.org/rfc/rfc7159.txt).

Primitives are only a subset of the JSON spec and they do not support floating point or exponents, instead only signed integers. Integers are stored in a 32-bit sized value, allowing for approximately -2 Billion to +2 Billion.

Floating points are trimmed to the integer value before the decimal point. Exponents have no handling.
Proper float support is planned, but not currently implemented.

### Arrays/Lists

Because of the 'everything-as-object' typing scheme, arrays can actually contain elements of any type or a mix of type.

The main difference between an Array/List and object in this case is that the array does not give its elements names and it is designed to be iterated over, whereas an object will have named fields.

An empty list is valid, but discouraged. If an array is empty, it MUST NOT contain any ',' delimiters.

Arrays as per JSON.

### Objects

An empty object is valid, but discouraged. If an object is empty, it MUST NOT contain any ',' delimiters.

Objects as per JSON.

### Imports

Imported files can actually contain any type that would be on the right-hand-side of the ':' token, however it is usually expected that they will be used to contain objects.

e.g.
```
    "SomeString": <importable.oj>,
```

#### Search algorithm

The string follows the usual search pattern followed by open(2), which means full or relative paths from the current working directory are processed.

However, if the file fails to open then the open is handled recursively, searching all non-hidden (and non . and .. ) sub directories down from the current working directory for a match. Directories are searched top-to-bottom, and the first match is used.

e.g. `<importable.oj>` will find both `./importable.oj` and `./folder/sub/importable.oj`, but `./importable.oj` is used over `./folder/sub/importable.oj`. 

### Queries

These are the core of why OJ is a programming language rather than just another markup/interchange language/format.

These can be considered somewhere between a C# property and an Objective-C message, but they can be used to replace functions/methods in some contexts too.

Queries only take a single argument, passed as an assignment. This accessible in the body via a keyword variable named `value`. This is always passed as reference.

Because query assignments (passing a variable to a query) operate the same as any variable assignment, they can be used in place of any variable assignment within another query including inside arrays. However, they MUST NOT be used within top-level object or array declarations.

### Other types

There are no other programmer-accessible types available in OJ. However there are some internal types for keyword handling.
