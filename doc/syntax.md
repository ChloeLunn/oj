# General Syntax

Generally, syntax is mostly the same as JSON, however you can access parts of objects where required in the usual dot-notation way of doing `Object.Field`. Other exceptions are noted below.

The `this.` in contexts like `this.Field` must be given for accessing fields of the local object.

The underlying JSON handling for the initial file load is all provided using a fork of my 'Junior' JSON parser with a couple of extensions for OJ-specifics, however within queries it's all handled by the OJ query implementation which is slightly different due to program flow controls and variable evaluation.

OJ, like Junior, does not support comments.
