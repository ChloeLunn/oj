# Assignments

In OJ, all invocation of operators/functions/queries are handled using the top-level assignment syntax. 

Every variable is passed into the assignment using pass-by-reference.

## Assignment Scope

Assignment only lasts for as long as a variable is in scope.

In short:

 - An unnamed variable being assigned only exists for that line.
 - A variable declared and assigned in a query only exists for that query unless added to an object or array
 - A variable/field declared in an object or array exists for as long as that object or array exists
 - An object or array exists for as long as it's parent exists

## Processing Order and Evaluation

Assignment order is handled right-to-left, such that the line `return: this.Bar.Add: "foo",` is evaluated thus:

1. A new un-named string object with value `"foo"` is temporarily allocated
2. The object created in step 1 is assigned to query `this.Bar.Add`
3. The resulting object from `this.Bar.Add` is assigned to the keyword `return`
4. `return` then causes the query to exit, passing the result in 3 out of the query 

Query evaluation normally only occurs when the query is accessed, that is to say when something is assigned to it or it is assigned to a simple object. There is more information in the specific sections on queries and special keywords about when this is not true.

## Assignments to ... 

### ... simple type

A "simple type" refers to any type other than a special keyword, operator query, or a user-defined query.

With simple types, if assigning a different type to the current type, this will also change the type of the variable to match the new assignment (with the exception of keywords and queries).

If needed, any fields/elements no longer referenced that were members of the variable when it was its previous type will be freed.

### ... new variables

Assignment to new variables, or a declaration of a new variable, is done the same as in JSON.
You first name the variable by putting the name in '"'s and then assign the value to that new name.

e.g. `"new_variable": 5,`

### ... primitives, strings, and booleans

Assignments to primitive variables are like any other programming language, the contents of the variable's value is changed to match the value being assigned to it.

e.g. `old_variable: 5,` will change the value in old_variable to be 5 and change its type to primitive if appropriate. 

### ... objects

Assignments to objects work much the same as variables, however the result is that whilst the assignee object is not a duplicate reference to the assigner/value object, the assignee object ends up with a copy of the references to the fields within the object being assigned.

That is, if we have two objects `foo` and `bar`, and `foo` has the field `egg`, then the assignment `bar: foo` does not make `bar` point to `foo`, but rather `bar` now contains the field `egg` instead of its previous fields, and access to `bar.egg` and `foo.egg` are accessing the same underlying object

To make a copy of all fields, the built-in operator `Duplicate` or special keyword `clone` may be used like so: `bar.Duplicate: foo` or `bar: clone: foo`. It is important to note when producing a copy using the `Duplicate` operator, that it is an operation on the *resulting* object, and not the object to be copied.

Duplicates and clones are subtly different. `Duplicate` creates a copy of all the fields within the object, however any further child fields or elements within those fields that are non-object simple types may keep their references (i.e `bar.egg.pan` may point to the same information as `foo.egg.pan`).

A `clone` result is a deep clone such that even child fields within `bar.egg` and `foo.egg`, or `bar.egg.pan` and `foo.egg.pan`, never reference the same information.

Please see the sections on `clone` and `Duplicate` for more information on their function.

### ... arrays

This happens the same as with objects above, however the 'fields' are instead the unnamed elements

### ... queries

When an assignment to a query is a simple type as above, then the query is evaluated with that assigned value passed by reference under the keyword 'value'

When an assignment to a query is another query, then the second query is evaluated before it's assignment.

When an assignment to a query is another query, then the second query can be passed as a reference without assignment using the keyword `ref`.

When an assignment to a query is a keyword, the keyword is evaluated as per the information in the section on that keyword.

### ... special keywords and built-in operator queries

Please see the section on the specific keyword you are assigning to for further information
