# if

e.g.
```
if: [ condition, if_code, sec_condition, elseif_code, else_code ]
```

could be evaluated to the C equivalent:

```
if (condition)
{
   if_code();
}
else if(sec_condition)
{
  elseif_code();
}
else
{
  else_code();
}
```

if_code, elseif_code, and else_code, like with other query evaluations, can be any type, but they can be made queries if they're to be code.

Condition/codes are processed in pairs. A hanging odd numbered item is taken to be the else.

You'll notice that the if works essentially as a query that's assigned an array. That's because that *is* how it works. The array is evaluated item by item just as it would be for any other assignment inside a query, and then passed to the if. Note that a variable (e.g. an array) that contains a reference to a query doesn't evaluate that query until it is accessed

E.g. the following shows how you could setup a different way of formatting this

```
if: [
    somevar.IsType: string,
    return: ?{
        return: null
    },
    return: ?{
        more_code: somevar,
        return: somevar
    }
]
```

It's not too easy to make sense of if you're new to OJ, but because the code being run is actually an assignment to the return special keyword, both branches of this will do a return in the query with their return value.

It's evaluated like so: `if: [ <new boolean>, <special return object with a new query as value>, <special return object with new query as value> ]`

If they weren't to return from the parent query then we could do something like

```
if: [
    somevar.IsType: string,
    ?{
        somevar: null
    },
    ?{
        more_code: somevar
    }
]
```

This is then evaluated like so: `if: [ <new boolean>, <new query>, <new query> ]`

We can also define an if like this where the code is a query object instead of in-line:
```
if: [
    somevar.IsType: string,
    query_var
]
```

Which is then evaluated like so: `if: [ <new boolean>, query_var ]`
