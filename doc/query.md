### Queries

Queries only take a single argument, passed as an assignment. This accessible in the body via a keyword variable named `value`. This is always passed as reference.

These are supposed to be considered somewhere between a C# property and an Objective-C message, but they can be used to replace functions/methods in some contexts too.

Because query assignments (passing a variable to a query) operate the same as any variable assignment, they can be used in place of any variable assignment within another query. They MUST NOT be used within top-level object or array declarations.

#### Query Example:

```
{
    "SomeString": "foo",
    "Bar": ?
        {
            "temp_var": "foo2",
            this.SomeString.Add: temp_var,
            return: this.SomeString.Add: "foo3"
        },
    "Foo": ?
        {
            return: this.Bar
        }
}
```

Here we have a top-level object with 3 fields: The first is a string, the next is a query function named 'Bar', and the last is a query named 'Foo'.

You will notice that syntax-ly, this is very similar to how a JSON object is done.

The 'Foo' query calls the 'Bar' query without an assignment This makes have a transparent access as if it *is* Bar, however as it ignores the 'value' it will never change the value in 'Bar'.

The 'Bar' query also ignores the 'value' whenever it's called, however in this case it acts more like a function. 

First, a temporary variable is created with the contents 'foo2' and this is appended to SomeString. Then a 'foo3' is appended to SomeString. The difference is that temp_var would be available until the end of the query, but the string with "foo3" is nameless and only exists that single time it is used.

Add is a built-in query that is available on certain object types (see below). As you can see from it's use above, Queries are accessed more-or-less the same as variables are.

Note that as per most JSON, the space between the statements and surrounding syntax is not relevant to the handling and whitespace may or may not be required.

The top level object should contain a 'Main' or 'Entry' (case insensitive) query if the file is to be executable.

If the query is being used as a 'get' and not a 'set' or assignment, then you can test for this by checking to see if value is null.

A query that does not have an explicit 'return' will return the last object returned by a prior query or assignment, this means in our example `return: this.SomeString.Add: "foo3"` and just `this.SomeString.Add: "foo3"` are both equally valid. 

All variable declarations inside a query (but not inside objects) are evaluated in the same way, even if they are inside an array or object inside that query, this will be noted more in the flow control section below, however you should note that a variable (e.g. an array) that contains a reference to a query doesn't evaluate that query until it is accessed.

e.g. `somevar: somequery: var` *will* evaluate somequery with the value var before assigning it to somevar, however `somevar: somequery` only assigns the reference to somequery to somevar.

#### Flow control

[If statements](if.md)

## Special/reserved words

All special words and built-in queries are represented as hidden types of object internally in OJ interpreter.

| Word                | Description                                                 |
| ------------------- | ----------------------------------------------------------- |
| [this](this.md)     | the object that the query is in                             |
| [system](system.md) | the operating system and calls exposed to OJ as an object   |
| [value](value.md)   | the value being assigned to the query                       |
| [return](return.md) | return from a query                                         |

There are also reserved type keywords. These cannot be used to declare a variable and are only for queries such as IsType/ToType.

| Word     | Description              |
| -------- | ------------------------ |
| string   | Is a JSON string         |
| int      | Is a primitive integer   |
| float    | Is a primitive float     |
| bool     | Is a boolean value       |
| null     | Is null                  |
| array    | Is an array of elements  |
| object   | Non-trivial object       |
| query    | A query property/message |

### Reserved/Built-in queries/fields/properties

| Type        | Query      | Returns         | Description                                                   |
| ----------- | ---------- | --------------- | ------------------------------------------------------------- |
| any/all     | IsEqual    | new bool        | check if two variables or objects are the same                |
| any/all     | IsType     | new bool        | check if the object type matches the given type               |
| any/all     | IsNothing  | new bool        | check if the object is non-existent                           |
| any/all     | ToType     | new object      | Try and convert the object to a different type  (like 'cast') |
| any/all     | Parent     | existing object | Return a reference to the parent of the provided object       |
| any/all     | Clone      | new object      | Create a copy of the object                                   |
| string      | Add/Append | changed object  | Append/Concat the end of the string with a second string      |
| string      | Remove     | changed object  | Remove a substring from the end of the string                 |
| string      | Replace    | changed object  | Replace a substring with another anywhere in the string       |
| string      | Length     | new primitive   | Returns the number of characters in the string                |
| string      | Contains   | new primitive   | Does the string contain the provided substring or characters  |
| primitive   | Add        | changed object  | Mathematical addition                                         |
| primitive   | Subtract   | changed object  | Mathematical subtraction                                      |
| primitive   | Multiply   | changed object  | Mathematical multiplication                                   |
| primitive   | Divide     | changed object  | Mathematical division                                         |
| primitive   | Modulo     | changed object  | Mathematical modulo                                           |
| object      | Field      | existing object | Find a field based on the provided string                     |
| array/list  | Count      | new primitive   | Returns the number of elements in the array                   |
| array/list  | ElementAt  | existing object | Returns the element reference at the index in the array       |
| array/list  | First      | existing object | Returns the element reference at the first index in the array |
| array/list  | Last       | existing object | Returns the element reference at the last index in the array  |
| array/list  | Add        | changed object  | Add an element to the array                                   |
| array/list  | Remove     | changed object  | Remove matching elements from the array                       |
| array/list  | Replace    | changed object  | Replace matching elements with another                        |
| array/list  | Contains   | new primitive   | Counts IsEqual successes on the elements in the array         |

Note: object refers to any non-trivial object.
