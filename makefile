COPT = -O2 -flto

lib_src = src/printj.c src/scanj.c src/fields.c src/malloc.c src/strings.c src/lists.c
lib_chng = src/printj.c src/scanj.c src/fields.c src/malloc.c src/strings.c src/lists.c include/json.h include/utf_helper.h

all: bin/oj

.PHONY oj: bin/oj
bin/oj: src/oj.cpp $(lib_chng)
	mkdir -p bin
	$(CXX) $(COPT) $(CFLAGS) -c -DTIMESTAMP_ISO=$(shell date -r $< -u +'"\"%Y-%m-%d\""') -std=c++11 src/oj.cpp -Iinclude/ -o bin/oj.o
	$(CXX) $(COPT) $(CFLAGS) -c -DTIMESTAMP_ISO=$(shell date -r $< -u +'"\"%Y-%m-%d\""') -std=c++11 src/query.cpp -Iinclude/ -o bin/query.o
	$(CXX) $(COPT) $(CFLAGS) -DTIMESTAMP_ISO=$(shell date -r $< -u +'"\"%Y-%m-%d\""') bin/oj.o bin/query.o -x c $(lib_src) -Iinclude/ -o bin/oj
	rm bin/oj.o bin/query.o
	strip bin/oj
